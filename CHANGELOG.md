# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v10.0.3 - 2024-12-19(10:00:06 +0000)

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] No notifications are sent

## Release v10.0.2 - 2024-12-12(15:03:16 +0000)

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): - [USP] No notifications are sent
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Boot event must be sent when NTP is synchronized

## Release v10.0.1 - 2024-11-25(10:50:48 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] The correct backend ctx must be used

## Release v10.0.0 - 2024-11-19(13:07:23 +0000)

### Break

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): - Vendor prefix non standard parameters

### Other

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): USP bus get_instances return
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Crash if PeriodicNotifInterval/PeriodicNotifTime changes

## Release v9.0.15 - 2024-10-24(20:08:00 +0000)

### Other

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): - Ambiorix USP-BE threshold support
- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): - Ambiorix USP-BE threshold support

## Release v9.0.14 - 2024-10-11(09:10:53 +0000)

### Other

- [libimtp](https://gitlab.com/prpl-foundation/components/core/libraries/libimtp): USP broker socket should be accessible to non-root users
- [libimtp](https://gitlab.com/prpl-foundation/components/core/libraries/libimtp): USP broker socket should be accessible to non-root users
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): USP broker socket should be accessible to non-root users

## Release v9.0.13 - 2024-10-01(08:30:52 +0000)

### Other

- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): CI: Disable squashing of open source commits
- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): [AMX] Disable commit squashing
- [libuspi](https://gitlab.com/prpl-foundation/components/core/libraries/libuspi): CI: Disable squashing of open source commits
- [libuspi](https://gitlab.com/prpl-foundation/components/core/libraries/libuspi): [AMX] Disable commit squashing
- [libuspprotobuf](https://gitlab.com/prpl-foundation/components/core/libraries/libprotobuf): CI: Disable squashing of open source commits
- [libuspprotobuf](https://gitlab.com/prpl-foundation/components/core/libraries/libprotobuf): [AMX] Disable commit squashing
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): CI: Disable squashing of open source commits
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [AMX] Disable commit squashing
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): CI: Disable squashing of open source commits
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [AMX] Disable commit squashing
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): CI: Disable squashing of open source commits
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [AMX] Disable commit squashing

## Release v9.0.12 - 2024-09-26(14:26:57 +0000)

## Release v9.0.11 - 2024-09-21(07:29:01 +0000)

### Other

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): CI: Disable squashing of open source commits

## Release v9.0.10 - 2024-09-20(07:24:44 +0000)

### Other

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): - [USP][CDRouter][Random] Some datamodel path are missing in USP hl-api tests
- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): - [USP][CDRouter][Random] Some datamodel path are missing in USP hl-api tests

## Release v9.0.9 - 2024-09-10(18:01:13 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Better shutdown script

## Release v9.0.8 - 2024-08-20(08:27:30 +0000)

## Release v9.0.7 - 2024-08-08(13:54:48 +0000)

## Release v9.0.6 - 2024-08-08(11:31:36 +0000)

### Fixes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): [AMX][USP] Avoid double USP connections

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): - [amx] Failing to restart processes with init scripts

## Release v9.0.5 - 2024-08-02(09:39:52 +0000)

## Release v9.0.4 - 2024-07-30(06:56:32 +0000)

## Release v9.0.3 - 2024-07-25(10:55:11 +0000)

### Fixes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Remove legacy subscribe interface function in favor of subscribe_v2
- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): [USP] Double operation complete notifications

### Changes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Make requires-device-prefix default value configurable

## Release v9.0.2 - 2024-07-24(06:41:46 +0000)

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [AMXB] Introduce depth parameter for subscriptions

## Release v9.0.1 - 2024-07-19(09:19:38 +0000)

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [TR181-Device]Bidirectional communication support between UBUS and IMTP

## Release v9.0.0 - 2024-07-18(08:51:16 +0000)

### New

- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): [USP] libusp library improvements
- [libuspi](https://gitlab.com/prpl-foundation/components/core/libraries/libuspi): [TR181-Device]Bidirectional communication support between UBUS and IMTP

### Other

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Make USP backend more compliant with USP specification

## Release v8.20.1 - 2024-07-04(20:24:15 +0000)

### Other

- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): [TR181-Device]Bidirectional communication support between UBUS and IMTP
- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): Key parameters without read-only in definition are write-once and must be reported as read-write in gsdm

## Release v8.20.0 - 2024-06-21(09:09:59 +0000)

### New

- [libimtp](https://gitlab.com/prpl-foundation/components/core/libraries/libimtp): Add new function to get connection flags

## Release v8.19.1 - 2024-06-15(07:04:30 +0000)

### Other

- [libimtp](https://gitlab.com/prpl-foundation/components/core/libraries/libimtp): [CI] add wnc-lvr5 compiler

## Release v8.19.0 - 2024-06-10(12:46:06 +0000)

### New

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Allow data model translations in USP backend

## Release v8.18.0 - 2024-06-07(07:26:53 +0000)

### New

- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): Add generic function to try to convert USP error to amxd status

### Fixes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Detection of bus context (USP) requires path to be dotted

## Release v8.17.5 - 2024-06-04(06:53:30 +0000)

## Release v8.17.4 - 2024-06-03(09:44:16 +0000)

## Release v8.17.3 - 2024-05-22(20:29:48 +0000)

### Other

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): [Security][ambiorix]Some libraries are not compiled with Fortify-Source
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [Security][ambiorix] Some libraries are not compiled with Fortify-Source

## Release v8.17.2 - 2024-05-16(20:59:54 +0000)

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [LocalAgent] define all key parameters as upc

## Release v8.17.1 - 2024-05-14(06:08:54 +0000)

### Fixes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): AMX USP backend must remove braces from operate name when received

## Release v8.17.0 - 2024-05-08(17:24:05 +0000)

### New

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Implement custom has function

### Changes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Methods must be called with braces

## Release v8.16.5 - 2024-05-07(08:44:23 +0000)

## Release v8.16.4 - 2024-05-02(13:41:43 +0000)

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): CLONE - LocalAgent doesn't start

## Release v8.16.3 - 2024-04-30(18:16:23 +0000)

### Other

- [libuspi](https://gitlab.com/prpl-foundation/components/core/libraries/libuspi): Move component to prpl-foundation gitlab
- [libuspprotobuf](https://gitlab.com/prpl-foundation/components/core/libraries/libprotobuf): Move component to prpl-foundation gitlab

## Release v8.16.2 - 2024-04-26(11:28:02 +0000)

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [CDROUTER][USP][REGRESSION] Cannot run USP tests on cdrouter due to the EndpointID new value

## Release v8.16.1 - 2024-04-24(10:55:17 +0000)

## Release v8.16.0 - 2024-04-24(09:38:02 +0000)

### New

- [mod-usp-registration](https://gitlab.com/prpl-foundation/components/core/modules/mod-usp_registration): Component added

## Release v8.15.2 - 2024-04-19(10:50:27 +0000)

### Changes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Allow multiple registrations for the same bus context
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Make amxb timeouts configurable

## Release v8.15.1 - 2024-04-15(13:37:41 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Set or Delete response does not return error codes

### Changes

- [tr181-uds](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-uds): Make amxb timeouts configurable
- [tr181-uspservices](https://gitlab.com/soft.at.home/usp/applications/tr181-uspservices): Make amxb timeouts configurable
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Make amxb timeouts configurable

## Release v8.15.0 - 2024-03-29(13:02:56 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Support all kind of "Cause" for Boot! Event

## Release v8.14.1 - 2024-03-27(19:57:49 +0000)

### Other

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] Extend documentation
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Extend documentation

## Release v8.14.0 - 2024-03-27(13:25:22 +0000)

### New

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): [USP] Add register retry mechanism

## Release v8.13.0 - 2024-03-26(20:28:02 +0000)

### New

- [tr181-uspservices](https://gitlab.com/soft.at.home/usp/applications/tr181-uspservices): Add tr181-device proxy odl files to components

## Release v8.12.0 - 2024-03-25(18:16:06 +0000)

### New

- [tr181-uds](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-uds): Add tr181-device proxy odl files to components

## Release v8.11.3 - 2024-03-22(13:16:00 +0000)

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Triggered! parameters need to be in a data variant
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] PeriodicNotifInterval changes are only applied after reboot

## Release v8.11.2 - 2024-03-14(17:10:27 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [CDROUTER][USP] Delete Message - Allow Partial True fails

## Release v8.11.1 - 2024-02-29(12:00:37 +0000)

### Other

- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): Rename BBF license to OBUSPA license
- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): [USP] Fix licensing issues for opensourcing USP libs
- [libusp](https://gitlab.com/prpl-foundation/components/core/libraries/libusp): Move component to prpl-foundation gitlab

## Release v8.11.0 - 2024-02-17(08:00:24 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Support of OnChange! Event

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): MTP status remains down

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Extend unit tests

## Release v8.10.0 - 2024-02-06(10:59:13 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Add tr181-device proxy odl files to components

## Release v8.9.1 - 2024-02-01(10:23:09 +0000)

## Release v8.9.0 - 2024-01-31(08:00:48 +0000)

### New

- [libimtp](https://gitlab.com/prpl-foundation/components/core/libraries/libimtp): [libimtp] Add file descriptor transfer functionality

## Release v8.8.4 - 2024-01-30(14:36:49 +0000)

### Fixes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): [USP] Issue with asynchronous calls

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP][B&R] PeriodicNotifInterval  parameter is not upgrade persistent

## Release v8.8.3 - 2024-01-25(09:26:57 +0000)

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [BaRt] Failed to handle multiple keys
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Remove sed from localagent startup script

## Release v8.8.2 - 2024-01-23(10:38:28 +0000)

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [BaRt] Failed to handle multiple keys
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Random String from EndpointId is not randomly enough
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Random String from EndpointId is not randomly enough

## Release v8.8.1 - 2024-01-18(16:30:21 +0000)

### Fixes

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] usp-endpoint needs to load usp backend

## Release v8.8.0 - 2024-01-17(14:45:07 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Add tr181-device proxy odl files to components

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [LocalAgent] Remove default MQTT client from odl file

## Release v8.7.2 - 2024-01-12(16:09:15 +0000)

### Fixes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): [USP] amxb_usp_poll_response returns with error randomly

## Release v8.7.1 - 2024-01-12(10:46:21 +0000)

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Add quick start guide to uspagent

## Release v8.7.0 - 2024-01-09(14:44:26 +0000)

### New

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Allow creating subscriptions via LocalAgent.Subscription dm

### Other

- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi): Remove circular dependency

## Release v8.6.13 - 2024-01-05(16:29:06 +0000)

### Other

- [libimtp](https://gitlab.com/prpl-foundation/components/core/libraries/libimtp): Update documentation
- [libimtp](https://gitlab.com/prpl-foundation/components/core/libraries/libimtp): Move library to prpl gitlab

## Release v8.6.12 - 2023-12-18(20:35:37 +0000)

### Other

- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi): [USP] Enable documentation generation for libuspi

## Release v8.6.11 - 2023-12-14(10:36:26 +0000)

### Fixes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): Deferred calls are cleaned up before everything is done

### Changes

- [mod-amxb-usp](https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_usp): [USP] Backend must be able to handle interleaved messages

## Release v8.6.10 - 2023-12-07(16:59:22 +0000)

## Release v8.6.9 - 2023-11-28(16:54:01 +0000)

## Release v8.6.8 - 2023-11-27(19:12:48 +0000)

## Release v8.6.7 - 2023-11-22(12:18:23 +0000)

## Release v8.6.6 - 2023-11-20(10:06:48 +0000)

## Release v8.6.5 - 2023-11-14(18:12:09 +0000)

## Release v8.6.4 - 2023-11-13(10:06:45 +0000)

## Release v8.6.3 - 2023-11-09(09:02:22 +0000)

### Fixes

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): Fix license headers in files
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): LocalAgent.MTP.{i}.Status not updated properly

### Changes

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): [USP] All plugins on host connect to the USP agent socket
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Move USP backend location

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Refactor libamxo - libamxp: move fd and connection management out of libamxo

## Release v8.6.2 - 2023-10-28(20:34:11 +0000)

## Release v8.6.1 - 2023-10-15(09:06:51 +0000)

### Changes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] LocalAgent EndpointID should be reboot persistent

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Fix license headers in files

## Release v8.6.0 - 2023-09-28(15:45:03 +0000)

### Other

- [tr181-uds](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-uds): Opensource component
- [tr181-uspservices](https://gitlab.com/soft.at.home/usp/applications/tr181-uspservices): Opensource component

## Release v8.5.0 - 2023-09-26(15:27:05 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Use latest protobuf schema
- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): [USP] Use latest protobuf schema
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Update USP error codes for registration

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] deregistered_path can be repeated
- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): [USP] deregistered_path can be repeated
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Fix license headers in files
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] onboard request not automatically sent by IB4 when MTP is re-enabled

### Other

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Fix license headers in files

## Release v8.4.3 - 2023-09-22(09:16:22 +0000)

## Release v8.4.2 - 2023-09-20(11:35:07 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Only send BulkData reports to subscribed controller
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Only send BulkData reports to subscribed controller

### Other

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [usp-endpoint] Fix baf output and remove () from condition check
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [usp-endpoint] Install uspa_definition.odl when CONFIG_SAH_SERVICES_USPE is enabled

## Release v8.4.1 - 2023-09-12(19:14:58 +0000)

## Release v8.4.0 - 2023-09-12(17:28:13 +0000)

### Security

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP][ACL] A remote USP controller is only allowed to access TR-181 dms

## Release v8.3.2 - 2023-09-07(15:02:05 +0000)

### New

- [mod-usp-cli](https://gitlab.softathome.com/ext_prpl/components/mod-usp-cli): Component added

## Release v8.3.1 - 2023-09-07(14:02:40 +0000)

### New

- [mod-usp-onboarding](https://gitlab.softathome.com/ext_prpl/components/mod-usp-onboarding): Component added
- [tr181-uds](https://gitlab.softathome.com/ext_prpl/components/tr181-uds): Component added
- [tr181-uspservices](https://gitlab.softathome.com/ext_prpl/components/tr181-uspservices): Component added

## Release v8.3.0 - 2023-09-07(09:18:49 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Our uspagent should use the USPServices data model

## Release v8.2.0 - 2023-09-01(12:17:26 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Retry onboarding in case it is not confirmed

### Fixes

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): [USP] Remove handshake from connect
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] OnBoardingComplete must be persistent
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Remove handshake from connect

### Changes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Trigger Device.Boot! event from uspagent

## Release v8.1.0 - 2023-08-31(12:37:27 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [PRPL][USP][Onboarding]MTP Connector usp messages missing
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] GSDM should return whether commands are (a)sync
- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): [USP] Add UDS connect record to library
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Add OnBoardingComplete parameter
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Add config flag to ignore partial
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] GSDM should return whether commands are (a)sync

### Fixes

- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi): [USP] messages are not published sometimes
- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): USP UDS connection is complete when handshake is done
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] Notify response published on wrong topic
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] messages are not published sometimes

### Changes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Limit length to 50 and use .. separators for oui_ext

### Other

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): Remove redundant line of code
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Update LocalAgent debug script
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] LocalAgent must wait for UnixDomainSockets plugin
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Recommend using allow_partial = true
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [AMX] Replace ubus-cli in debuginfo script

## Release v8.0.2 - 2023-07-19(13:56:39 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Retry IMTP connection in case of failure
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Prevent segmentation fault after IMTP disconnect

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Add SocketStatus parameter to data model
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [KPN][USP] Boot! event is not as expected
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Bus timeout when fetching Device object on boot

## Release v8.0.1 - 2023-07-14(14:48:30 +0000)

## Release v8.0.0 - 2023-07-14(13:24:55 +0000)

### Breaking

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): [IMTP] Implement IMTP communication as specified in TR-369
- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi): [IMTP] Implement IMTP communication as specified in TR-369
- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): [IMTP] Implement IMTP communication as specified in TR-369
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [IMTP] Implement IMTP communication as specified in TR-369
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [IMTP] Implement IMTP communication as specified in TR-369
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [IMTP] Implement IMTP communication as specified in TR-369

### Other

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] Add config variables for init script of usp-endpoint

## Release v7.2.0 - 2023-07-07(12:16:34 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Trigger 'Sendonboardrequest()' via TR-069/TR-369 Parameter
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Change USP EndpointId authority-scheme to "oui"

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Update log message for allow_partial=false

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): - [KPN][USP] Boot! event is not as expected
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [CR9HF] - Multiple tr181-localagent sessions running
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): - [HTTPManager][WebUI] Create plugin's ACLs permissions
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): EnableMDNS is true by default in spec
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [CR9HF] - Multiple uspagent sessions running

## Release v7.1.0 - 2023-06-16(07:58:59 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): It must be possible to build partially failed add responses
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [Security][USP] Add ACLs for subscriptions to USP agent
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Support Error Code: 7025 'Object exists with duplicate key'
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [Security][USP] Add ACLs for get instances to USP agent
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [Security][USP] Add ACLs for get supported dm to USP agent

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Input arguments must be JSON encoded
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] allow_partial=false must be rejected
- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): [USP] Agent stuck doing blocking amxb_resolves
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP][MQTT] Missing unique keys for MQTT data model
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP][MQTT] Missing unique keys for MQTT data model

### Changes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Add specific error codes for get instances
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] allow_partial=false must be rejected

### Other

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Extend README with runtime dependencies

## Release v7.0.0 - 2023-06-08(16:00:18 +0000)

### Removed

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Component removed

## Release v6.2.0 - 2023-05-12(11:20:52 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] It must be possible to use protected methods
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] It must be possible to use protected methods

### Fixes

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] Controllers with are added with an invalid EndpointID
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): HGWKPN-2285 [USP][Set Message] Error code not as expected

## Release v6.1.1 - 2023-05-10(14:24:12 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Agent stuck doing blocking amxb_resolves

## Release v6.1.0 - 2023-05-09(14:38:09 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): gsdm missing arguments for commands and events

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP][AMX] GSDM needs a ValueChangeType
- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): [USP] Get with depth not working as expected
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] tr181-localagent should use amxp_dir_owned_make
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT] Client Status no longer goes to Error
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Missing index number in ObjectCreation notifications

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Add missing event parameters to data model

## Release v6.0.0 - 2023-05-02(13:56:54 +0000)

### Breaking

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): Subscriptions with the USP backend no longer work

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Add NotifType AmxNotification for ambiorix events
- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi): [USP] Add NotifType AmxNotification for ambiorix events
- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): [USP] Add NotifType AmxNotification for ambiorix events
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Issue NET-4657: [USP] Add NotifType AmxNotification for ambiorix events
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] usp-endpoint must be able to broker amx subscriptions
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] uspagent must be able to broker amx subscriptions

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [USP][DNS] Resolution is not triggered systematically when Wan was NOK and recover
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Improve ACL handling with adds

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Also update Alias
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Extend defaults with Controller MTP

## Release v5.5.1 - 2023-04-24(08:11:15 +0000)

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [V12][23.04.12-D] RG onboarding behaves differently on VDSL lines and on FIBER (possible DNS resolution timing issue on VDSL?)

## Release v5.5.0 - 2023-04-14(15:12:32 +0000)

### New

- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi): [USP] Add support for subscriptions to usp-endpoint
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] Add support for subscriptions to usp-endpoint
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Send ForceReconnect based on AutoReconnect parameter

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Stopping tr181-mqtt before tr181-localagent resets references
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Make sure BrokerAddress is used in connect
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Disabling second controller breaks IMTP connection for first
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP][Regression] Cannot find IMTP con for enabled controller

### Changes

- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi): [USP] Port subscription changes to libuspi
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Move AutoReconnect to LocalAgent.MTP.i.MQTT.
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] Port subscription changes to usp-endpoint
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Add requests with search paths will be allowed

### Other

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [uspe] Remove default endpoint id for uspe
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Extend subscription unit tests

## Release v5.4.0 - 2023-04-06(08:57:15 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): USP agent must know when to ForceReconnect() client

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Must use hostname in connect for certificate checking

### Changes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT][USP] Make client auto reconnect configurable

## Release v5.3.1 - 2023-04-05(08:56:47 +0000)

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Remove var dumps from libusp
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Let uspagent save LastValue for parameter subscriptions
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [KPN][USP] Boot! event notification is not as expected
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Incorrect parameter attributes
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Lists of strings must be of type csv_string_t
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [ACS] [V12]: No Request sent from Device side and Inconsistent onboarding behaviour of V12 on MQTT server
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT] DNS resolving can still block
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Correctly save last value of parameter subscriptions
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Must be able to forward notifications on IMTP
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Dynamically adding LocalAgent.MTP. instances does not work
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [KPN][USP] Boot! event notification is not as expected
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Discovery object should not be protected

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [Config] enable configurable coredump generation
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Use sah_libc-ares instead of opensource_c-ares
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Exposure of Device.MQTT.Client. instances for both Uber/BDD/Lansec and USP
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Documentation should be written for adding subscriptions to containers

## Release v5.3.0 - 2023-03-14(15:39:22 +0000)

## Release v5.2.0 - 2023-03-05(08:06:53 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP][CDROUTER] The NotifExpiration limit is not respected

## Release v5.1.6 - 2023-02-28(10:00:37 +0000)

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [V12] [USP] GET Message Object = Device. - Retry mechanism is not working

## Release v5.1.5 - 2023-02-24(15:17:11 +0000)

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT] Remove protected attribute from Interface parameter

## Release v5.1.4 - 2023-02-23(12:40:14 +0000)

### Changes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [USP] Redirection to Management Broker without the need of ForceReconnect() command

## Release v5.1.3 - 2023-02-22(11:08:56 +0000)

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [PRPL] Make MSS work on prpl config

## Release v5.1.2 - 2023-02-20(12:44:29 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Parameter paths must be fetched with depth=0

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Add tr181-mqtt/tr181-localagent/uspagent into processmonitor

## Release v5.1.1 - 2023-02-16(15:27:47 +0000)

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Add tr181-mqtt/tr181-localagent/uspagent into processmonitor
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Create direct pcb connection to tr181-mqtt
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Add tr181-mqtt/tr181-localagent/uspagent into processmonitor
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Setup direct pcb connection for tr181-localagent

## Release v5.1.0 - 2023-02-14(19:04:39 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Include advertisement module in localagent

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Box12 doesn't onboard in Motive
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Creation of LocalAgent.Subscription from save file can fail

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Update unit tests after recent changes

## Release v5.0.7 - 2023-02-10(19:35:07 +0000)

## Release v5.0.6 - 2023-02-07(14:55:07 +0000)

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [SAHPairing] Make sahpairing work with MQTT client

## Release v5.0.5 - 2023-02-06(09:15:41 +0000)

## Release v5.0.4 - 2023-02-02(09:48:02 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Remove doc target from uspagent

## Release v5.0.3 - 2023-01-31(13:12:26 +0000)

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT][USP] Overlapping reconnects can cause a segmentation fault

## Release v5.0.2 - 2023-01-23(20:32:13 +0000)

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [KPN SW2][Security]Restrict ACL of admin user

## Release v5.0.1 - 2023-01-19(15:30:26 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP Agent][Amx]USP request returns 'invalid path' all the time

## Release v5.0.0 - 2023-01-13(12:03:03 +0000)

### Breaking

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [KPN][USP] max_depth has no effect on the Get Message

### New

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): [USP][AMX] Add support for asynchronous invokes to USP backend

### Fixes

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): Handle command output args for non-backend processes

### Changes

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): [KPN][USP] max_depth has no effect on the Get Message
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [KPN][USP] max_depth has no effect on the Get Message
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [KPN][USP] max_depth has no effect on the Get Message

### Other

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): [SAHPairing] Make sahpairing work with MQTT client

## Release v4.1.5 - 2023-01-10(16:12:24 +0000)

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Get requests with valid search expressions must return successful
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [TR181-MQTT Client] tr181-mqtt wants a non empty ClientID with MQTT 3.1.1
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Handle command output args for non-backend processes
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [Bulkdata][USP] Controller parameter must be set
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [uspagent] dm:OperationComplete created too late
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Get requests with valid search expressions must return successful
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): LocalAgent.MTP.i.Status must be Up if MQTT client is connected

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [KPN SW2][Security]Restrict ACL of admin user

## Release v4.1.4 - 2022-12-13(13:15:51 +0000)

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Add interop with different MQTT Broker version

## Release v4.1.3 - 2022-12-09(13:59:50 +0000)

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Triggered event needs an exclamation mark
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [Config] coredump generation should be configurable

## Release v4.1.2 - 2022-12-07(16:15:02 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Allow invoking commands without braces

## Release v4.1.1 - 2022-12-06(13:34:43 +0000)

## Release v4.1.0 - 2022-12-02(12:17:38 +0000)

### New

- [mod-amxb-usp](https://gitlab.com/soft.at.home/usp/modules/amxb_backends/amxb_usp): Component added

## Release v4.0.1 - 2022-11-25(10:07:14 +0000)

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] GSDM response cannot be extracted properly

## Release v4.0.0 - 2022-11-24(15:15:03 +0000)

### Removed

- [mod-usp-cli](https://gitlab.com/soft.at.home/usp/modules/amx_cli/mod-usp-cli): Component removed

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [AMX] Apply new amxd_path_setf formatting
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [AMX] Apply new amxd_path_setf formatting
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [AMX] Apply new amxd_path_setf formatting

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Add extra controller instance
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Add IMTP defaults to tr181-localagent
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Rename EndpointID
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Update IMTP defaults

## Release v3.11.0 - 2022-11-23(12:00:24 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Extend error code conversion
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [Security][USP] Add ACLs for operate to USP agent

### Other

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Opensource component

## Release v3.10.1 - 2022-11-17(15:14:03 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP][tr181-localagent]Local Agent MTP status is down even if connected and subscribed
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): USP agent requires LocalAgent data model

## Release v3.10.0 - 2022-11-16(13:55:53 +0000)

### New

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Add TcpSendMem parameter to Client object

## Release v3.9.4 - 2022-11-10(16:31:33 +0000)

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Confusion around USP version
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Missing Periodic! event in data model
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): No response must be send when send_resp is false

### Other

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): [Packet Interception] Create the new Packet Interception component

## Release v3.9.3 - 2022-11-08(08:47:48 +0000)

### Fixes

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] uspi_con_read can return with value 1

## Release v3.9.2 - 2022-10-29(07:44:10 +0000)

### Changes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): It must be possible to accept multiple MQTT connections per Client
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): USP agent/controller must indicate it is interested in USP messages
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [ACS][V12] Setting of VOIP in a single SET does not enable VoiceProfile
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): uspagent must indicate it is interested in USP messages

## Release v3.9.1 - 2022-10-25(06:39:49 +0000)

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT] Segmentation fault in tr181-mqtt after enabling client
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Add extra logging
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] usp-endpoint should have separate entrypoint

## Release v3.9.0 - 2022-10-20(13:21:36 +0000)

### New

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): Need separate type for JSON encoded MQTT properties

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Fix unit tests of libusp after changes to libamxd

### Changes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [ACS][V12] Setting of VOIP in a single SET does not enable VoiceProfile

## Release v3.8.1 - 2022-10-18(14:40:28 +0000)

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Values of reference parameters should start with Device.
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Values of reference parameters should start with Device. (revert)
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Permission denied due to ControllerTrust mismatch

## Release v3.8.0 - 2022-10-13(14:35:58 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Add method to get controller MTP info from dm
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USPAgent][MQTT Client] It must be possible to share a single MQTT Client with multiple USP Controller instances

## Release v3.7.2 - 2022-10-11(11:07:01 +0000)

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): AssignedRole parameter must have a Device. prefix
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Strip Device. prefix from AssignedRole

## Release v3.7.1 - 2022-10-10(08:33:41 +0000)

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Missing MTPNumberOfEntries parameter
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] IMTP Status parameter is Down when it should be Up

## Release v3.7.0 - 2022-09-30(11:44:20 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] IMTP should use LocalAgent.Subscription for subscriptions

### Fixes

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] usp-endpoint exits when no bus ctx is found
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] uspagent does not respond correctly with EndpointID

## Release v3.6.0 - 2022-09-21(07:43:13 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): MQTT ClientID must have the same value as LocalAgent.EndpointID

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Disable opensource CI
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Use sah-lib-mosquitto-dev in debian package dependencies
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [USP] Open source tr181-mqtt
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [BART] Create ambiorix bart module
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [BART] Create ambiorix bart module

## Release v3.5.0 - 2022-09-15(13:31:11 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Unit tests need to be added for the MTP of type IMTP

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] [Add_Msg] The AddResp does not contain the elements in the unique keymap under the OperationSuccess
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] Agent must assign default values
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] [Add_Msg] The AddResp does not contain the elements in the unique keymap under the OperationSuccess
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP][CDROUTER] Agent does not reject messages that do not contain their to_id in the USP record

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] LIBDIR should be STAGING_LIBDIR to avoid conflicts

## Release v3.4.1 - 2022-09-08(07:13:06 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Latest version of USP breaks MQTT communication

## Release v3.4.0 - 2022-09-06(13:19:34 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Upstep version of libuspprotobuf
- [libuspi](https://gitlab.com/soft.at.home/usp/libraries/libuspi): Component added
- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): [USP] Upstep version of libuspprotobuf
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Add MTP of type IMTP
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Add MTP of type IMTP

### Fixes

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): Blocking read should not spam logs

## Release v3.3.0 - 2022-08-30(12:37:24 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Add MTP of type IMTP

## Release v3.2.1 - 2022-08-26(06:19:41 +0000)

### Changes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Register message does not need to contain URI

## Release v3.2.0 - 2022-08-25(14:03:22 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] The Periodic! event should be ported from previous code base

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): PeriodicNotifInterval value should be at least 1
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [ACS][V12] ValueChange Subscriptions only sent for last parameter in ReferenceList
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Slots can be triggered when subscription was already removed

## Release v3.1.1 - 2022-08-16(12:54:01 +0000)

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [ACS][V12] RG does not send a Boot! event for the second Reboot() command

## Release v3.1.0 - 2022-08-04(07:27:00 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Value change on Device.DeviceInfo.SoftwareVersion after firmware upgrade
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Value change on Device.DeviceInfo.SoftwareVersion after firmware upgrade

## Release v3.0.0 - 2022-08-02(06:55:10 +0000)

### Removed

- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): Component removed

## Release v2.9.1 - 2022-07-22(08:43:08 +0000)

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Certificate and config to connect to FUT backend
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] ACL files cannot be retrieved when role paths don't end with a dot

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [KPN][ROB] CPU increased has been observed

## Release v2.9.0 - 2022-07-14(17:24:28 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Add latest USP error codes

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] LIBDIR should be STAGING_LIBDIR to avoid conflicts

### Changes

- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): [USP] A dot must be added to reference paths
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] A dot must be added to reference paths
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] A dot must be added to reference paths
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Make forwarding of output arguments in a USP message more generic

## Release v2.8.2 - 2022-07-12(15:44:15 +0000)

### Fixes

- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): List index out of range
- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): TR181 Crash on CR2bis
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT] TLS parameters should be persistent

### Changes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] It must be possible to set string parameters starting with a plus
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] It must be possible to set string parameters starting with a plus

## Release v2.8.1 - 2022-07-04(11:03:07 +0000)

### Fixes

- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): Don't strip Device. when using ref in search path

## Release v2.8.0 - 2022-06-30(07:26:40 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP][KPN] Set ResponseTopicConfigured to hdm topic

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] tr181-mqtt function CreateListenSocket should be called on MQTT object

### Changes

- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): [USP][KPN] Set ResponseTopicConfigured to hdm topic

## Release v2.7.2 - 2022-06-22(07:08:46 +0000)

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Support TLS for MQTT.Client instance used for USP

## Release v2.7.1 - 2022-06-14(08:15:56 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP][CDROUTER] The Agent responds with an invalid supported protocol to the controller

## Release v2.7.0 - 2022-06-09(12:15:50 +0000)

### New

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP Agent] Implement TransferComplete functionality

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Problem with loading defaults file
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] The USP agent should store its persistent parameters periodically
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] MQTT.Client.{i}.ForceReconnect() can time out

### Changes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Add CommandKey and Requestor to args of async operations

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [amx][MQTT][Client] The MQTT client must support a (tr181) retransmission scheme

## Release v2.6.0 - 2022-06-03(14:23:02 +0000)

### New

- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Implement GetInstances request and response

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Missing function to handle get_instances

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Component should have a debian package
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Issue: amx/usp/applications/tr181-localagent#1 Component should have a debian package
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): ODL option import-dbg should be disabled
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): The USP Agent must be opensourced to gitlab.com/soft.at.home
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [packages][debian] Correct makefile

## Release v2.5.0 - 2022-05-25(14:47:04 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP Agent] Implement GetInstances message

### Fixes

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected
- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): [libfwinterface] Pipeline doesn't stop when memory leaks are detected
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

### Other

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Add libamxj to deps
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v2.4.1 - 2022-05-11(07:25:25 +0000)

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Update header file
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT] Topic must be writable after creation
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] No response from USP agent

## Release v2.4.0 - 2022-05-05(07:52:11 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP Agent] Implement GetInstances message
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Implement operate command

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Correctly install odl files
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] USP commands with braces are invoked synchronously

## Release v2.3.0 - 2022-05-02(14:11:31 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Separate BBF and SAH code

### Fixes

- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [MQTT] Clients connecting with the same ID cause conflicts
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT] Clients connecting with the same ID cause conflicts

### Other

- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): Enable auto opensourcing

## Release v2.2.0 - 2022-04-25(13:32:58 +0000)

### New

- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): [Device] Support Sending Boot! event
- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): [USP][MQTT] MQTT topic needs to be set dynamically
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP][MQTT] MQTT topic needs to be set dynamically
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [amx][MQTT][Client] The MQTT client must support a (tr181) retransmission scheme

### Fixes

- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): Cannot find shared object on box
- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): Makefile install target is broken
- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): Only include prefix odl
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP][MQTT] Update order of entrypoints
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] It must be possible to override odl files
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [USP] LocalAgent.Subscription is no longer persistent
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): amxp timer works with miliseconds
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [AMX] Hidden parameter cannot be saved correctly
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [Buildsystem] Pipes are stripped from odl files when they are installed [fix]
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Device prefix prevents finding the IMTP context
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] USP notifications contain swapped EndpointIDs

## Release v2.1.0 - 2022-04-06(12:47:01 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [Device] Support Sending Boot! event

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Operate response can contain zero output args
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Event parameters can contain empty strings
- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): [USP] Onboarding module unsubscribes too soon
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): [Device] Support Sending Boot! event

## Release v2.0.0 - 2022-03-29(11:39:27 +0000)

### Breaking

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USPAgent] Split agent in datamodel part and function part

### New

- [mod-usp-onboarding](https://gitlab.com/soft.at.home/usp/modules/mod_usp_onboarding): Component added
- [tr181-localagent](https://gitlab.com/soft.at.home/usp/applications/tr181-localagent): Component added

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [MQTT] Legacy code needs to be removed

### Changes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [GetDebugInformation] Add data model debuginfo in component services
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [USP] Re-enable data model persistency in uspagent and tr181-mqtt
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [GetDebugInformation] Add data model debuginfo in component services

## Release v1.9.3 - 2022-03-23(13:52:20 +0000)

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Doc generation for tr181-mqtt component (amx)
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Set KeepAliveTime to 0 by default [revert]

## Release v1.9.2 - 2022-03-15(15:48:57 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [ACL][USP] ACL files must be located in writable directory
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] [Set_msg] No response from the agent after sending a Set message

## Release v1.9.1 - 2022-03-15(11:38:47 +0000)

### Fixes

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Set KeepAliveTime to 0 by default

### Other

- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): Enable core dumps by default
- [tr181-mqtt](https://gitlab.com/soft.at.home/plugins/tr181-mqtt): [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v1.9.0 - 2022-02-24(18:25:53 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] It must be possible to build deregister messages

### Fixes

- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): Paths field in deregister must be plural
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Cannot build register message with multiple paths

### Other

- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v1.8.0 - 2022-02-17(18:18:29 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Implement a discovery service
- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Handle allow partial set
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Handle allow partial set

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Cannot create USP Error messages with parameter errors
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Cannot create USP Error messages with parameter errors

### Other

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v1.7.0 - 2022-02-03(18:24:03 +0000)

### New

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): [IMTP] TLV of type endpoint_id is needed

### Fixes

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): Need to reread large IMTP messages
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Remove impt_tlv_type_msgid

### Other

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): Solve open source issues
- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v1.6.0 - 2022-01-28(19:54:15 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): It must be possible to generate a unique endpoind ID for a USP endpoint

### Fixes

- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): [IMTP] Need to reread large IMTP messages

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Add extra logging after acl verification

## Release v1.5.1 - 2022-01-20(16:43:50 +0000)

### Changes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Component downstepped from v0.3.0 to v0.2.12

## Release v1.5.0 - 2022-01-14(21:26:11 +0000)

### New

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Handle allow partial set

### Changes

- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): Replace supports_add boolean with shared boolean

## Release v1.4.1 - 2022-01-11(20:14:27 +0000)

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Error with notifications when installing 2 applications in the same time

## Release v1.4.0 - 2022-01-07(17:11:12 +0000)

### New

- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): tr181-mqtt has trouble connecting due to DNS issue at boot

### Fixes

- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): [MQTT] Properties should only be added for MQTT 5.0

## Release v1.3.1 - 2021-12-21(11:52:16 +0000)

### Fixes

- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): [MQTT] tr181-mqtt should not try to extract msg id from TLV

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP Agent] Add unittest for on board request
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Agent must publish responses on controller's reply-to topic

## Release v1.3.0 - 2021-12-16(15:14:54 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Implement OnBoardRequest

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Missing notification after async operation

### Other

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] libusp is spamming a useless log message

## Release v1.2.1 - 2021-12-13(16:43:12 +0000)

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): OperationComplete notification may contain no output_args
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Remove references to libmosquitto_poc
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): Controller must not crash when receiving USP Error messages

### Changes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): Extend unit tests for OperationComplete notifications
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Agent must publish responses on controller's reply-to topic
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [USP] Agent must publish responses on controller's reply-to topic
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): [USP] Agent must publish responses on controller's reply-to topic

## Release v1.2.0 - 2021-12-08(19:16:34 +0000)

### New

- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): [USP] Implement a discovery service

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP Agent] USP messages on sop should be send over pcb sysbus with Device prefix still present

### Changes

- [libuspprotobuf](https://gitlab.com/soft.at.home/usp/libraries/libprotobuf): [USP] Make protobuf compiler version check less strict

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Issue: amx/usp/applications/uspagent#41 [DOC] Ambiorix events must contain a separate htable for parameters
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USPAgent] Support Controller.BootParameter.{i}. feature

## Release v1.1.2 - 2021-11-30(13:16:33 +0000)

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] Version check should be less strict
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Incorrect subscription filter for Events
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] The correct bus context must be used for operations

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP Agent] USP messages on sop should be send over pcb sysbus with Device prefix still present

## Release v1.1.1 - 2021-11-23(14:27:51 +0000)

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Get supported protocol message should be handled on requests instead of responses
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Avoid segmentation fault on exit

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP Agent] Retrieve correct mtp information when a notify retry happens
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Change startup order
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Update documentation
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Handle subscriptions of objects under Device.

## Release v1.1.0 - 2021-10-28(10:08:52 +0000)

### New

- [libimtp](https://gitlab.com/soft.at.home/usp/libraries/libimtp): Add documentation for TLV messages

## Release v1.0.0 - 2021-10-14(11:36:43 +0000)

### Breaking

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP][ACL] Add access control verification to USP agent

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [ACL] Add default ACL configuration per services

### Fixes

- [libusp](https://gitlab.com/soft.at.home/usp/libraries/libusp): [USP] allow_partial should be ignored
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Add unit tests to uspagent
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [AMX][USP] Prevent double initialization in uspagent
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): Segmentation fault when disconnecting client
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): [ACL] Add default ACL configuration per services
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): [USP] MQTT clients should send ping messages to stay connected

### Other

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP][ACL] Operator must have access while mapper is missing
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP][ACL] Operator must have access while mapper is missing
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Add README and configuration guidelines for USP agent
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): Add example ACL files

## Release v0.4.0 - 2021-09-24(10:04:09 +0000)

### New

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): Issue:  NET-2985 [USPAgent][Device] Handle Device.LocalAgent USP messages
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USPAgent] Implementation of the ControllerTrust featureDev controllertrust
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] Configure defaults for communicating with orange controller
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): Extend logging for MQTT properties

### Fixes

- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] USP agent should use first bus context
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] The USP Agent and MQTT client should have the USP backend as run time dependency
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [AMX] Debian packages for ambiorix plugins should create symlinks to amxrt
- [uspagent](https://gitlab.com/soft.at.home/usp/applications/uspagent): [USP] LocalAgent Reference parameters should use the same action callbacks
- [usp-endpoint](https://gitlab.com/soft.at.home/usp/applications/usp-endpoint): [AMX] Debian packages for ambiorix plugins should create symlinks to amxrt
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): Issue #27: Remove references to libmosquitto_poc from README
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): [USP] The USP Agent and MQTT client should have the USP backend as run time dependency
- [tr181-mqtt](https://gitlab.com/prpl-foundation/components/core/plugins/tr181-mqtt): [AMX] Debian packages for ambiorix plugins should create symlinks to amxrt
